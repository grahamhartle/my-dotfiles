
export ZDOTDIR=$HOME/.config/zsh

HISTFILE=$ZDOTDIR/.zsh_history
HISTSIZE=10000
SAVEHIST=$HISTSIZE

# editor

export EDITOR="nvim"
export VISUAL="nvim"

# bindkey -v
# export KEYTIMEOUT=1

# auto completions
autoload -Uz compinit && compinit

# alias
alias pip=pip3
alias vim='/usr/local/bin/nvim'

# virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Development
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
source /usr/local/bin/virtualenvwrapper.sh

# options
setopt auto_cd
setopt auto_list
setopt auto_menu
setopt always_to_end
setopt hist_ignore_all_dups
setopt hist_reduce_blanks
setopt inc_append_history
setopt share_history
setopt correct_all
setopt interactive_comments

# styles
zstyle ':completion:*' menu select
zstyle ':completion:*' group-name ''
zstyle ':completion:::::' completer _expand _complete _ignored _approximate
zstyle ':fuzzy-search-and-edit:editor' use-visual yes

# misc
# export SHELL='/usr/local/bin/zsh'
. /usr/local/etc/profile.d/z.sh
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"
export PATH="$PATH:/usr/local/opt/gettext/bin"
export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
export SPELL='/usr/local/bin/aspell'

export PATH="/usr/local/sbin:$PATH"
export PATH="/usr/local/opt/ruby/bin:$PATH"
export PATH="/usr/local/opt/gettext/bin:$PATH"
export PATH="/usr/local/opt/icu4c/bin:$PATH"
export PATH="/usr/local/opt/icu4c/sbin:$PATH"
export PATH="/usr/local/opt/python@3.8/bin:$PATH"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

source /usr/local/Cellar/fzf/0.22.0/shell/completion.zsh
source /usr/local/Cellar/fzf/0.22.0/shell/key-bindings.zsh
source $HOME/.config/zsh/plugins/git.plugin.zsh

# antigen
source /usr/local/share/antigen/antigen.zsh

antigen use robbyrussell/oh-my-zsh

antigen bundle plugins/z
antigen bundle plugins/osx
antigen bundle plugins/git
antigen bundle plugins/colored-man-pages

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-completions

antigen theme agnoster

antigen apply

