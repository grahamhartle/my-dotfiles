set number
set autoindent
set expandtab
set tabstop=4
set shiftwidth=4
set backspace=2
set smarttab
set mouse=a
set t_Co=256
syntax enable
set background=dark
colorscheme onedark

" Pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

" airline
let g:airline_powerline_fonts = 1
let g:airline_theme = 'papercolor'

if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

